﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> CreateAsync(T entity)
        {
            Guid result = Guid.Empty;

            try
            {
                var dataCollection = (ICollection<T>)Data;
                dataCollection.Add(entity);
                
                result = entity.Id;
            }
            catch (Exception ex)
            {
                result = Guid.Empty;
            }

            return Task.FromResult(result);
        }

        public Task<Guid> DeleteAsync(Guid id)
        {
            Guid result = Guid.Empty;

            try
            {
                var element = Data.FirstOrDefault(x => x.Id == id);
                var dataCollection = (ICollection<T>)Data;
                if (dataCollection.Remove(element))
                    result = id;
            }
            catch (Exception ex)
            {
                result = Guid.Empty;
            }

            return Task.FromResult(result);
        }

        public Task<bool> UpdateAsync(T entity)
        {
            bool result = false;

            try
            {
                var element = Data.FirstOrDefault(x => x.Id == entity.Id);
                var dataCollection = (ICollection<T>)Data;
                if (dataCollection.Remove(element))
                {
                    dataCollection.Add(entity);
                    if (Data.FirstOrDefault(x => x.Id == entity.Id) != null)
                        result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return Task.FromResult(result);
        }
    }
}