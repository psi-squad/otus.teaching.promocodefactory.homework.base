﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Mappers
{
    public static class DtoMappers
    {
        public static Employee EmployeeRequestToEmployee(EmployeeRequest employeeRequest, List<Role> roles)
        {
            Employee employee;
            try
            {
                employee = new Employee()
                {
                    Id = employeeRequest.Id,
                    FirstName = employeeRequest.FirstName,
                    LastName = employeeRequest.LastName,
                    Email = employeeRequest.Email,
                    AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
                    Roles = roles.Where(r => employeeRequest.RoleIds.Contains(r.Id.ToString())).ToList()
                };
            }
            catch
            {
                employee = null;
            }

            return employee;
        }
    }
}
