﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController: ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /*
                /// <summary>
                /// Создать сотрудника
                /// </summary>
                /// <returns></returns>
                [HttpPost]
                public async Task<Guid> CreateEmployeeAsync(string firstName, string lastName, string email)
                {
                    Employee employee = new Employee() { Id = Guid.NewGuid(), FirstName = firstName, LastName = lastName, Email = email };

                    var result = await _employeeRepository.CreateAsync(employee);

                    return result;
                }
        */

        /// <summary>
        /// Создать сотрудника
        /// Не передавать поле id в EmployeeRequest
        /// </summary>
        /// <returns>Guid созданного сотрудника, иначе пустой Guid</returns>
        [HttpPost]
        public async Task<Guid> CreateEmployeeAsync(EmployeeRequest employeeRequest)
        {
            List<Role> roles = _roleRepository.GetAllAsync().Result.ToList();
            Employee employee = DtoMappers.EmployeeRequestToEmployee(employeeRequest, roles);
            employee.Id = Guid.NewGuid();

            var result = await _employeeRepository.CreateAsync(employee);

            return result;
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns>Guid удаленного сотрудника, иначе пустой Guid</returns>
        [HttpDelete]
        public async Task<Guid> DeleteEmployeeAsync(Guid id)
        {
            var result = await _employeeRepository.DeleteAsync(id);

            return result;
        }

        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <returns>true при успешном изменении сотрудника, иначе false</returns>
        [HttpPut]
        public async Task<bool> UpdateEmployeeAsync(EmployeeRequest employeeRequest)
        {
            List<Role> roles = _roleRepository.GetAllAsync().Result.ToList();
            Employee employee = DtoMappers.EmployeeRequestToEmployee(employeeRequest, roles);

            var result = await _employeeRepository.UpdateAsync(employee);

            return result;
        }

    }
}